/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */
 
package edu.uwplatt.cnelson.spotdsl.validation

import edu.uwplatt.cnelson.spotdsl.spotDsl.gFileTransformation
import edu.uwplatt.cnelson.spotdsl.spotDsl.gModel
import edu.uwplatt.cnelson.spotdsl.spotDsl.gOperation
import edu.uwplatt.cnelson.spotdsl.spotDsl.gProjectTransformation
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotAssign
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotCheckpoint
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotComment
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotCommentScope
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotDelete
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotOmp
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotPut
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotRename
import java.util.ArrayList
import org.eclipse.xtext.validation.Check

/**
 * This class contains custom validation rules. 
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class SpotDslValidator extends AbstractSpotDslValidator {

	public static val INVALID_NAME = 'invalidName'
	private ArrayList<String> varNames = new ArrayList<String>();
	private static boolean help;

	/**
	 * Loops through the model and validates each operation
	 */
	@Check
	def checkVariables(gModel model) {
		varNames.clear()
		for (gProjectTransformation projTransform : model.transformations)
			for (gFileTransformation fileTransform : projTransform.fileTransformations){
				if(fileTransform.comment.exists)
					fileTransform.comment.operate
				else
					for (gOperation operation : fileTransform.operations)
						operation.assignOperation
			}
				
	}
	
	/**
	 * Assigns validation to the proper operation
	 */
	def void assignOperation(gOperation operation) {
		if(operation.assign.exists)
			operation.assign.operate()
		else if(operation.put.exists)
			operation.put.operate()
		else if(operation.rename.exists)
			operation.rename.operate()
		else if(operation.delete.exists)
			operation.delete.operate()
		else if(operation.comment.exists)
			operation.comment.operate()
		else if(operation.omp.exists)
			operation.omp.operate()
		else if(operation.checkpoint.exists)
			operation.checkpoint.operate()
	}

	/**
	 * Validates and provides help for an Assign function
	 */
	def void operate(gSpotAssign data) {
		var name = ""
		
		val isNew = data.knew.exists
		
		if(isNew)
			name = data.knew.name
		else
			name = data.existing.name
			
		if(varNames.contains(name))
			error("Cannot re-data the value of a variable after instantiation", data.eContainer, data.eContainingFeature)
		else
			varNames.add(name)
			
		if(!help)
			return;
			
		info("This assigns a variable to a statement.\nSyntax: <VarName> = <Statement>", data.eContainer, data.eContainingFeature)
		
	}
	
	/**
	 * Validates and provides help for a Put function
	 */
	def void operate(gSpotPut data) {
		if(!help)
			return;
		
		info("This inserts a new statement before, at, or after an existing one.\nSyntax: put <NewStatement> <before/at/after> <ExistingStatement>", data.eContainer, data.eContainingFeature)
	}
	
	/**
	 * Validates and provides help for a Rename function
	 */
	def void operate(gSpotRename data) {
		if(data.statementRename.exists) {
			error("Unimplemented feature. Wait for a future update.", data.eContainer, data.eContainingFeature)
			return;
		}
		
		if(!help)
			return;
		
		info("This renames a function.\nSyntax: rename(\"New Name\")", data.eContainer, data.eContainingFeature)
	}
	
	/**
	 * Validates and provides help for a Delete function
	 */
	def void operate(gSpotDelete data) {
		if(!help)
			return;
		
		info("This deletes an existing statement.\nSyntax: delete(<ExistingStatement>)", data.eContainer, data.eContainingFeature)
	}
	
	/**
	 * Validates and provides help for a function-level Comment function
	 */
	def void operate(gSpotComment data) {
		if(!help)
			return;
			
		if(data.statementComment.exists)
			info("This attaches a comment to a statement.\nSyntax: comment(ExistingStatement, \"Comment\")", data.eContainer, data.eContainingFeature)
		else if(data.scopeComment.exists)
			info("This attaches a comment to a function.\nSyntax: comment(\"Comment\")", data.eContainer, data.eContainingFeature)
	}
	
	/**
	 * Validates and provides help for a file-level Comment function
	 */
	def void operate(gSpotCommentScope data) {
		if(!help)
			return;
		
		info("This attaches a comment to a file.\nSyntax: comment(\"Comment\")", data.eContainer, data.eContainingFeature)
	}
	
	/**
	 * Apologizes for my shortcomings
	 */
	def void operate(gSpotOmp data) {
		error("Unimplemented feature. Wait for a future update.", data.eContainer, data.eContainingFeature)
	}
	
	/**
	 * Validates and provides help for a Checkpoint function
	 */
	def void operate(gSpotCheckpoint data) {
		if(!help)
			return;
		
		info("This inserts checkpoints (periodical saves).\nAuto Syntax: ckpt()\nManual Syntax: ckpt(\"var_1\", \"var_2\", ... \"var_n\")", data.eContainer, data.eContainingFeature)
	}
	
	/**
	 * Enables the help feature. Triggered from SpotDslGenerator
	 */
	def static void enableHelp() {
		help = true;
	}
	
	/**
	 * Disables the help feature. Triggered from SpotDslGenerator
	 */
	def static void disableHelp() {
		help = false;
	}

	/**
	 * Util to check if an object exists
	 */
	def boolean exists(Object obj) {
		obj !== null
	}
}
