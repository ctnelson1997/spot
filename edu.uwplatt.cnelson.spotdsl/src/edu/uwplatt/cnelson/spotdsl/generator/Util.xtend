/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator

import edu.uwplatt.cnelson.spotdsl.generator.statements.Statement
import edu.uwplatt.cnelson.spotdsl.spotDsl.gArgumentListOptional
import java.util.ArrayList

/**
 * Provides utilities that are occasionally used.
 */
class Util {

	/**
	 * Checks if an object exists
	 */
	def static boolean exists(Object obj){
		obj !== null
	}
	
	/**
	 * Mends (trims) a multi-line character sequence into a single line.
	 */
	def static mend(CharSequence str)'''
		�str.toString.replaceAll("\n", "").replaceAll("\r", "")�
	'''
	
	/**
	 * Converts from a list of optional arguments to an array of strings
	 */
	 def static ArrayList<String> convert(gArgumentListOptional list) {
	 	var arr = new ArrayList<String>();
	 	if(!list.vars.exists)
	 		return arr;
	 	for(String value : list.vars){
	 		arr.add(value);
	 	}
	 	return arr;
	 }
	
	/**
	 * Builds a for loop that iterates through a global vector
	 */
	def static buildForLoop(Statement statement, CharSequence contents)'''
		�Util.mend('''for(unsigned int i = 0; i < �statement.global�.size(); i++) {''')�
			�contents�
		}
	'''
}