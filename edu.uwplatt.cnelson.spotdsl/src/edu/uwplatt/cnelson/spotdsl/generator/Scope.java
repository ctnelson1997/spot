/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator;

import edu.uwplatt.cnelson.spotdsl.spotDsl.gFile;
import edu.uwplatt.cnelson.spotdsl.spotDsl.gFunction;

public class Scope{
	public final gFile FILE;
	public final gFunction FUNCTION;
	
	/**
	 * Creates the scope of a statement with unmutable fields
	 */
	public Scope(gFile file, gFunction function){
		FILE = file;
		FUNCTION = function;
	}
}
