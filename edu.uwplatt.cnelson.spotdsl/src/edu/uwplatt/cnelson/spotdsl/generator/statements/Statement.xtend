/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.statements

import edu.uwplatt.cnelson.spotdsl.generator.DocumentedGeneration
import edu.uwplatt.cnelson.spotdsl.generator.Scope
import edu.uwplatt.cnelson.spotdsl.generator.Util

abstract class Statement implements DocumentedGeneration {
	
	public final Scope scope;
	
	/**
	 * Required Grammar counterpart and scope
	 */
	new(Scope scope){
		this.scope = scope;
	}
	
	/**
	 * Gets the global vector name for the statement
	 */
	def CharSequence getGlobal();
	
	/**
	 * Appends a (presumably) variable onto the vector
	 */
	def protected appendVector(CharSequence str)'''
		�Util.mend('''�getGlobal�.push_back(�str�);''')�
	'''
	
	/**
	 * Provides the accessor for the vector
	 */
	def protected access(CharSequence str)'''
		�str�[i]
	'''
	
	/**
	 * Util to check if an object exists
	 */
	def protected boolean exists(Object obj){
		return (obj !== null)
	}
	
	/**
	 * Supresses the CharSequence output from an operation to null
	 */
	def protected CharSequence supress(Object obj){}
}