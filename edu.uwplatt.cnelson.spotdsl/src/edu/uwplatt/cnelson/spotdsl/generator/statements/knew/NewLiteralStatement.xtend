/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.statements.knew

import edu.uwplatt.cnelson.spotdsl.generator.Scope
import edu.uwplatt.cnelson.spotdsl.generator.Util
import edu.uwplatt.cnelson.spotdsl.generator.statements.ExistingStatement
import edu.uwplatt.cnelson.spotdsl.generator.statements.NewStatement
import edu.uwplatt.cnelson.spotdsl.spotDsl.gNewLiteralStatement

class NewLiteralStatement extends NewStatement {
	
	private gNewLiteralStatement data;
	
	new(gNewLiteralStatement data, Scope scope, ExistingStatement dependency) {
		super(scope, dependency)
		this.data = data;
	}
	
	override build()'''
		�'''buildExprStatement(buildOpaqueVarRefExp("�data.statement�", �accessScope�))'''.appendVector�
	'''
	
	override comment() '''
		�Util.mend('''// Builds the literal statement "�data.statement�"''')�
	'''
}
