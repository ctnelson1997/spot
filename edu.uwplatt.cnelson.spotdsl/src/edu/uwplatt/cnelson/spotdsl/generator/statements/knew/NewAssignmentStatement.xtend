/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.statements.knew

import edu.uwplatt.cnelson.spotdsl.generator.Scope
import edu.uwplatt.cnelson.spotdsl.generator.Util
import edu.uwplatt.cnelson.spotdsl.generator.statements.ExistingStatement
import edu.uwplatt.cnelson.spotdsl.generator.statements.NewStatement
import edu.uwplatt.cnelson.spotdsl.spotDsl.gNewAssignmentStatement

class NewAssignmentStatement extends NewStatement {
	
	private gNewAssignmentStatement data;
	
	new(gNewAssignmentStatement data, Scope scope, ExistingStatement dependency) {
		super(scope, dependency)
		this.data = data;
	}
	
	override build()'''
		�IF data.type.integer�
			�'''buildAssignStatement(buildVarRefExp("�data.name�", �accessScope�), buildIntVal(�data.value�))'''.appendVector�
		�ELSEIF data.type.float�
			�'''buildAssignStatement(buildVarRefExp("�data.name�", �accessScope�), buildFloatVal(�data.value�))'''.appendVector�
		�ELSEIF data.type.string�
			�'''buildAssignStatement(buildVarRefExp("�data.name�", �accessScope�), buildStringVal(�data.value�))'''.appendVector�
		�ENDIF�
	'''
	
	override comment() '''
		�Util.mend('''// Builds a statement that sets �data.name� to �data.value�''')�
	'''
	
}
