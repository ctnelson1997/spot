/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.statements

import edu.uwplatt.cnelson.spotdsl.generator.Scope
import edu.uwplatt.cnelson.spotdsl.generator.Util

abstract class NewStatement extends Statement {
	
	private final int STATEMENT_NUM;
	
	protected ExistingStatement dependency;
	
	private static int numExistingStatements;
	
	new(Scope scope, ExistingStatement dependency) {
		super(scope)
		this.dependency = dependency;
		STATEMENT_NUM = numExistingStatements++;
	}
	
	def void setDependency(ExistingStatement dependency){
		this.dependency = dependency;
	}
	
	def construct() '''
		�Util.buildForLoop(dependency, build)�
	'''
	
	def accessScope() '''
		�dependency.global.access�->get_scope()
	'''
	
	def CharSequence build();
	
	def static void reset(){
		numExistingStatements = 0;
	}
	
	override getGlobal()'''
		gNew�STATEMENT_NUM�
	'''
}