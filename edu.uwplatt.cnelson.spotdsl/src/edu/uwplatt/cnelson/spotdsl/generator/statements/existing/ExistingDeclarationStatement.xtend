/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.statements.existing

import edu.uwplatt.cnelson.spotdsl.generator.Scope
import edu.uwplatt.cnelson.spotdsl.generator.statements.ExistingStatement
import edu.uwplatt.cnelson.spotdsl.spotDsl.gExistingDeclarationStatement

class ExistingDeclarationStatement extends ExistingStatement {
	
	private gExistingDeclarationStatement data;
	
	new(gExistingDeclarationStatement data, Scope scope) {
		super(scope)
		this.data = data;
	}
	
	override buildInspector() '''
		SgVariableDeclaration* vDecl = isSgVariableDeclaration(statement);
		if(vDecl == NULL)
			continue;
		
		Rose_STL_Container<SgInitializedName*>& variableList = vDecl->get_variables();
		Rose_STL_Container<SgInitializedName*>::iterator var = variableList.begin();
		while(var != variableList.end()){
			if((*var)->get_name().getString() == "�data.name�"){
				�"statement".appendVector�
			}
			var++;
		}
	'''
	
	override buildTraversalComment() '''
		Gets declaration statement "�data.name�"
	'''
	
}