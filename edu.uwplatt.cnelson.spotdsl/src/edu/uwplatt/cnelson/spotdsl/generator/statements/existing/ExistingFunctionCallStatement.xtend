/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.statements.existing

import edu.uwplatt.cnelson.spotdsl.generator.statements.ExistingStatement
import edu.uwplatt.cnelson.spotdsl.spotDsl.gExistingFunctionCallStatement
import edu.uwplatt.cnelson.spotdsl.generator.Scope

class ExistingFunctionCallStatement extends ExistingStatement {
	
	private gExistingFunctionCallStatement data;
	
	new(gExistingFunctionCallStatement data, Scope scope) {
		super(scope)
		this.data = data;
	}
	
	override buildInspector() '''
		SgExprStatement* exprStmt = isSgExprStatement(statement);
		if(exprStmt == NULL)
			continue;
		
		SgFunctionCallExp* funcCall = isSgFunctionCallExp(exprStmt->get_expression());
		if(funcCall == NULL)
			continue;
		
		SgFunctionRefExp* funcRef = isSgFunctionRefExp(funcCall->get_function());
		if(funcRef == NULL)
			continue;
		
		if(funcRef->get_symbol()->get_declaration()->get_name().getString() == "�data.name�"){
			�"statement".appendVector�
		}
	'''
	
	override buildTraversalComment() '''
		Gets function call "�data.name�"
	'''
	
}