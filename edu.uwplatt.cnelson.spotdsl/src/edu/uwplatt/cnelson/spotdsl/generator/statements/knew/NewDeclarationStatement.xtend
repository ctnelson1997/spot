/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.statements.knew

import edu.uwplatt.cnelson.spotdsl.generator.Scope
import edu.uwplatt.cnelson.spotdsl.generator.Util
import edu.uwplatt.cnelson.spotdsl.generator.statements.ExistingStatement
import edu.uwplatt.cnelson.spotdsl.generator.statements.NewStatement
import edu.uwplatt.cnelson.spotdsl.spotDsl.gNewDeclarationStatement

class NewDeclarationStatement extends NewStatement {
	
	private gNewDeclarationStatement data;
	
	new(gNewDeclarationStatement data, Scope scope, ExistingStatement dependency) {
		super(scope, dependency)
		this.data = data;
	}
	
	override build() '''
		�IF data.type.integer�
			�IF data.value === null�
				�'''buildVariableDeclaration("�data.name�", buildIntType(), NULL, �accessScope�)'''.appendVector�
			�ELSE�
				�'''buildVariableDeclaration("�data.name�", buildIntType(), buildAssignInitializer(buildIntVal(�data.value�), buildIntType()), �accessScope�)'''.appendVector�
			�ENDIF�
		�ELSEIF data.type.float�
			�IF data.value === null�
				�'''buildVariableDeclaration("�data.name�", buildFloatType(), NULL, �accessScope�)'''.appendVector�
			�ELSE�
				�'''buildVariableDeclaration("�data.name�", buildFloatType(), buildAssignInitializer(buildFloatVal(�data.value�f), buildFloatType()), �accessScope�)'''.appendVector�
			�ENDIF�
		�ELSEIF data.type.string�
			�IF data.value === null�
				�'''buildVariableDeclaration("�data.name�", buildStringType(new SgExpression(NULL)), NULL, �accessScope�)'''.appendVector�
			�ELSE�
				�'''buildVariableDeclaration("�data.name�", buildStringType(new SgExpression(NULL)), buildAssignInitializer(buildStringVal("�data.value�"), buildStringType(new SgExpression(NULL))), �accessScope�)'''.appendVector�
			�ENDIF�
		�ENDIF�
	'''
	
	override comment() '''
		�IF data.value === null�
			�Util.mend('''// Builds a variable "�data.name�" with no initial value''')�
		�ELSE�
			�Util.mend('''// Builds a variable "�data.name�" with an initial value of "�data.value�"''')�
		�ENDIF�
		
	'''
}
