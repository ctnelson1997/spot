/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.statements.knew

import edu.uwplatt.cnelson.spotdsl.generator.Scope
import edu.uwplatt.cnelson.spotdsl.generator.Util
import edu.uwplatt.cnelson.spotdsl.generator.statements.ExistingStatement
import edu.uwplatt.cnelson.spotdsl.generator.statements.NewStatement
import edu.uwplatt.cnelson.spotdsl.spotDsl.gNewFunctionCallStatement

class NewFunctionCallStatement extends NewStatement {
	
	private gNewFunctionCallStatement data;
	
	new(gNewFunctionCallStatement data, Scope scope, ExistingStatement dependency) {
		super(scope, dependency)
		this.data = data;
	}
	
	override build()'''
		SgFunctionDeclaration* func = findDeclarationStatement<SgFunctionDeclaration> (project, "�data.name�", NULL, false);
		
		�buildExprList�
		
		�'''buildExprStatement(buildFunctionCallExp(new SgFunctionSymbol(func), buildExprListExp(expressions)))'''.appendVector�
	'''
	
	def private buildExprList()'''
		vector<SgExpression*> expressions;
		�IF data.exists && data.args.exists && data.args.args.exists�
			�FOR arg : data.args.args�
				�IF arg.value.exists�
					�IF arg.value.type.integer�
						�Util.mend('''expressions.push_back(buildIntVal(�arg.value.value�));''')�
					�ELSEIF arg.value.type.float�
						�Util.mend('''expressions.push_back(buildFloatVal(�arg.value.value�f));''')�
					�ELSEIF arg.value.type.string�
						�Util.mend('''expressions.push_back(buildStringVal("�arg.value.value�"));''')�
					�ENDIF�
				�ELSE�
					�Util.mend('''expressions.push_back(buildVarRefExp("�arg.variable.name�", �accessScope�));''')�
				�ENDIF�
		    �ENDFOR�
		�ENDIF�
	'''
	
	override comment() '''
		�Util.mend('''// Builds a function call to �data.name�''')�
	'''
}
