/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.statements.existing

import edu.uwplatt.cnelson.spotdsl.generator.statements.ExistingStatement
import edu.uwplatt.cnelson.spotdsl.spotDsl.gExistingAssignmentStatement
import edu.uwplatt.cnelson.spotdsl.generator.Scope

class ExistingAssignmentStatement extends ExistingStatement {
	
	private gExistingAssignmentStatement data;
	
	new(gExistingAssignmentStatement data, Scope scope) {
		super(scope)
		this.data = data;
	}
	
	override buildInspector() '''
		SgExpression* lhs;
		if(!isAssignmentStatement(statement,&lhs))
			continue;
			
		
		SgVarRefExp* varRef = isSgVarRefExp(lhs);
		if(varRef == NULL)
			continue;
		
		if(strcmp(varRef->get_symbol()->get_name().str(), "�data.name�") == 0){
			�"statement".appendVector�
		}
	'''
	
	override buildTraversalComment() '''
		Gets assignment statement "�data.name�"
	'''
}