/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.statements.existing

import edu.uwplatt.cnelson.spotdsl.generator.Scope
import edu.uwplatt.cnelson.spotdsl.generator.statements.ExistingStatement

/**
 * This class simply returns the location of the found function.
 * It is used in operations such as CommentFunctionOperation
 */
class FunctionLocator extends ExistingStatement {
	
	new(Scope scope) {
		super(scope)
	}
	
	override buildInspector() '''
		�"decl".appendVector�
		�escape�
	'''
	
	override buildTraversalComment() '''
		Locates a function (to be used by an operation)
	'''
	
}