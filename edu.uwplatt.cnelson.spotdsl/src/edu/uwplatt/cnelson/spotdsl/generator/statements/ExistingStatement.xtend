/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.statements

import edu.uwplatt.cnelson.spotdsl.generator.Scope
import edu.uwplatt.cnelson.spotdsl.generator.Util

abstract class ExistingStatement extends Statement {
	
	private final int STATEMENT_NUM;
	
	private static int numExistingStatements;
	
	new(Scope scope) {
		super(scope);
		STATEMENT_NUM = numExistingStatements++;
	}
	
	/**
	 * Escapes outside of the current loop in ROSE
	 */
	def protected escape()'''
		goto outer�STATEMENT_NUM�;
	'''

	/**
	 * Generates the ROSE code to get the existing statement
	 */
	def gen() '''
		for(unsigned int i = 0; i < blocks.size(); i++) {
			SgStatementPtrList listOfStatements;
			SgScopeStatement* check_scope = blocks[i];
			SgForStatement* check_for = isSgForStatement(check_scope);
			SgWhileStmt* check_while = isSgWhileStmt(check_scope);
			SgBasicBlock* check_block = isSgBasicBlock(check_scope);
			if(check_for != NULL)
				listOfStatements = isSgBasicBlock(check_for->get_loop_body())->get_statements();
			else if(check_while != NULL)
				listOfStatements = isSgBasicBlock(check_while->get_body())->get_statements();
			else if(check_block != NULL)
				listOfStatements = check_block->get_statements();
			else
				continue;
			
			for(unsigned int j = 0; j < listOfStatements.size(); j++) {
				SgStatement* statement = isSgStatement(listOfStatements[j]);
				if(statement == NULL)
					continue;
				
				�buildInspector�
			}
		}
		
		outer�STATEMENT_NUM�: ;
	'''
	
	/**
	 * Gets the actual code to check for the statement
	 */
	def CharSequence buildInspector();
	
	/**
	 * Generates the ROSE comment for the traversal identification
	 */
	override comment() '''
		�IF scope.FILE.wildcard && scope.FUNCTION.wildcard�
			�Util.mend('''//�buildTraversalComment� in any file and in any function''')�
		�ELSEIF !scope.FILE.wildcard && scope.FUNCTION.wildcard�
			�Util.mend('''//�buildTraversalComment� in file "�scope.FILE.name�" and in any function''')�			
		�ELSEIF scope.FILE.wildcard && !scope.FUNCTION.wildcard�
			�Util.mend('''//�buildTraversalComment� in any file and in function "�scope.FUNCTION.name�"''')�
		�ELSE�
			�Util.mend('''//�buildTraversalComment� in file "�scope.FILE.name�" and in function "�scope.FUNCTION.name�"''')�		
		�ENDIF�
	'''
	
	/**
	 * Gets the actual comment for the traversal build
	 */
	def CharSequence buildTraversalComment();
	
	/**
	 * Used on each new save to reset
	 */
	def static void reset(){
		numExistingStatements = 0;
	}
	
	/**
	 * Returns the global variable for the existing statement
	 */
	override getGlobal()'''
		gExist�STATEMENT_NUM�
	'''
}