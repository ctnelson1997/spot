/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator

import edu.uwplatt.cnelson.spotdsl.generator.operations.CommentFileOperation
import edu.uwplatt.cnelson.spotdsl.generator.operations.CommentFunctionOperation
import edu.uwplatt.cnelson.spotdsl.generator.operations.CommentStatementOperation
import edu.uwplatt.cnelson.spotdsl.generator.operations.DeleteOperation
import edu.uwplatt.cnelson.spotdsl.generator.operations.PutOperation
import edu.uwplatt.cnelson.spotdsl.generator.operations.RenameFunctionOperation
import edu.uwplatt.cnelson.spotdsl.generator.operations.RenameStatementOperation
import edu.uwplatt.cnelson.spotdsl.generator.operations.checkpoint.AutoCheckpointOperation
import edu.uwplatt.cnelson.spotdsl.generator.operations.checkpoint.ManualCheckpointOperation
import edu.uwplatt.cnelson.spotdsl.generator.operations.omp.OmpAtomicOperation
import edu.uwplatt.cnelson.spotdsl.generator.operations.omp.OmpBarrierOperation
import edu.uwplatt.cnelson.spotdsl.generator.operations.omp.OmpCriticalOperation
import edu.uwplatt.cnelson.spotdsl.generator.operations.omp.OmpParallelOperation
import edu.uwplatt.cnelson.spotdsl.generator.operations.types.CheckpointOperation
import edu.uwplatt.cnelson.spotdsl.generator.operations.types.FileOperation
import edu.uwplatt.cnelson.spotdsl.generator.operations.types.FunctionOperation
import edu.uwplatt.cnelson.spotdsl.generator.statements.ExistingStatement
import edu.uwplatt.cnelson.spotdsl.generator.statements.NewStatement
import edu.uwplatt.cnelson.spotdsl.generator.statements.Statement
import edu.uwplatt.cnelson.spotdsl.generator.statements.existing.ExistingAssignmentStatement
import edu.uwplatt.cnelson.spotdsl.generator.statements.existing.ExistingDeclarationStatement
import edu.uwplatt.cnelson.spotdsl.generator.statements.existing.ExistingFunctionCallStatement
import edu.uwplatt.cnelson.spotdsl.generator.statements.existing.FunctionLocator
import edu.uwplatt.cnelson.spotdsl.generator.statements.knew.NewAssignmentStatement
import edu.uwplatt.cnelson.spotdsl.generator.statements.knew.NewDeclarationStatement
import edu.uwplatt.cnelson.spotdsl.generator.statements.knew.NewFunctionCallStatement
import edu.uwplatt.cnelson.spotdsl.generator.statements.knew.NewLiteralStatement
import edu.uwplatt.cnelson.spotdsl.spotDsl.gExistingStatement
import edu.uwplatt.cnelson.spotdsl.spotDsl.gFileTransformation
import edu.uwplatt.cnelson.spotdsl.spotDsl.gModel
import edu.uwplatt.cnelson.spotdsl.spotDsl.gNewStatement
import edu.uwplatt.cnelson.spotdsl.spotDsl.gOperation
import edu.uwplatt.cnelson.spotdsl.spotDsl.gProjectTransformation
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotAssign
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotCheckpoint
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotComment
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotDelete
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotOmp
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotPut
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotRename
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotVarExistingStatement
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotVarNewStatement
import java.util.ArrayList
import java.util.HashMap

class Registrar {
	
	private gModel model;
	private ArrayList<Statement> statements = new ArrayList<Statement>();
	private ArrayList<FunctionOperation> functionOperations = new ArrayList<FunctionOperation>();
	private ArrayList<FileOperation> fileOperations = new ArrayList<FileOperation>();
	private ArrayList<CheckpointOperation> checkpointOperations = new ArrayList<CheckpointOperation>();
	
	/**
	 * Registers the model and generates the structure
	 */
	new(gModel model) {
		this.model = model;
		this.gen();
	}
	
	/*
	 * BEGIN STRUCTURE GENERATION
	 */
	 
	/**
	 * Seeks out each operation in a SPOT project and hands it to the delegator
	 * This both discerns the statements and queues the operations.
	 */
	def private void gen(){
		reset()
		for(gProjectTransformation projTransform : model.transformations)
			for(gFileTransformation fileTransform : projTransform.fileTransformations)
				if(fileTransform.comment.exists)
					fileOperations.add(new CommentFileOperation(fileTransform.comment))
				else
					for(gOperation operation : fileTransform.operations)
						operation.assignOperation(new Scope(projTransform.file, fileTransform.function))
	}
	
	/**
	 * Resets data (statements, operations, and statement count) for each new save
	 */
	def private void reset() {
		NewStatement.reset();
		ExistingStatement.reset();
		StatementRegistrar.reset();
	}

	/**
	 * Discerns and delegates the handler of a SPOT operation
	 */
	def private void assignOperation(gOperation operation, Scope scope){
		if(operation.assign.exists)
			operation.assign.operate(scope)
		else if(operation.put.exists)
			operation.put.operate(scope)
		else if(operation.rename.exists)
			operation.rename.operate(scope)
		else if(operation.delete.exists)
			operation.delete.operate(scope)
		else if(operation.comment.exists)
			operation.comment.operate(scope)
		else if(operation.omp.exists)
			operation.omp.operate(scope)
		else if(operation.checkpoint.exists)
			operation.checkpoint.operate(scope)
	}
	
	/**
	 * Used to associate a SPOT variable with its corresponding statement
	 */
	def private void operate(gSpotAssign data, Scope scope){
		StatementRegistrar.store(data, scope);
	}
	
	/**
	 * Delegates the operation for a "put" statement.
	 * Passes the statements into the list and operation into the queue.
	 */
	def private void operate(gSpotPut data, Scope scope){
		var existingStatement = StatementRegistrar.assign(data.existing, scope);
		var newStatement = StatementRegistrar.assign(data.knew, scope, existingStatement);
		newStatement.add();
		existingStatement.add();
		functionOperations.add(new PutOperation(newStatement, existingStatement, data.location))
	}
	
	/**
	 * Enqueues a rename operation
	 */
	def private void operate(gSpotRename data, Scope scope){
		var statement = new FunctionLocator(scope);
		statement.add();
		if(data.statementRename.exists)
			functionOperations.add(new RenameStatementOperation(statement, data.statementRename.oldName, data.statementRename.newName))
		else if(data.scopeRename.exists)
			functionOperations.add(new RenameFunctionOperation(statement, scope.FUNCTION.name, data.scopeRename.name))
	}
	
	/**
	 * Enqueues a delete operation
	 */
	def private void operate(gSpotDelete data, Scope scope){
		var statement = StatementRegistrar.assign(data.statement, scope);
		statement.add();
		functionOperations.add(new DeleteOperation(statement));
	}
	
	/**
	 * Delegates between a comment on a particular statement versus
	 * a comment on the entire function and passes it into the
	 * statement list and operation queue
	 */
	def private void operate(gSpotComment data, Scope scope){
		if(data.statementComment.exists){
			var existingStatement = StatementRegistrar.assign(data.statementComment.statement, scope);
			existingStatement.add();
			functionOperations.add(new CommentStatementOperation(existingStatement, data.statementComment.comment));
		} else if(data.scopeComment.exists) {
			var existingStatement = new FunctionLocator(scope);
			existingStatement.add();
			functionOperations.add(new CommentFunctionOperation(existingStatement, data.scopeComment.comment));
		}
	}
	
	/**
	 * Delegates an OMP operation and enqueues it
	 */
	def private void operate(gSpotOmp data, Scope scope) {
		if(data.atomic.exists){
			var statement = StatementRegistrar.assign(data.atomic.statement, scope);
			statement.add();
			functionOperations.add(new OmpAtomicOperation(statement));
		} else if(data.barrier.exists) {
			var statement = StatementRegistrar.assign(data.barrier.statement, scope);
			statement.add();
			functionOperations.add(new OmpBarrierOperation(statement));
		} else if(data.critical.exists) {
			var statement = StatementRegistrar.assign(data.critical.statement, scope);
			statement.add();
			functionOperations.add(new OmpCriticalOperation(statement));
		} else if(data.parallel.exists) {
			var statement = StatementRegistrar.assign(data.parallel.statement, scope);
			statement.add();
			functionOperations.add(new OmpParallelOperation(statement));
		}
	}
	
	/**
	 * Delegates a Checkpoint operation and enqueues it
	 */
	def private void operate(gSpotCheckpoint data, Scope scope){
		var freq = 1 // sets the frequency to default (1) if not specified
		if(data.freq > 0) // means it was specified
			freq = data.freq
			
		if(!data.args.exists || data.args.vars.isEmpty)
			checkpointOperations.add(new AutoCheckpointOperation(scope, freq))
		else
			checkpointOperations.add(new ManualCheckpointOperation(scope, Util.convert(data.args), freq))
		
	}
	
	/*
	 * END STRUCTURE GENERATION
	 */
	
	/*
	 * START ACCESSORS
	 */
	
	/**
	 * Checks if transformations include checkpointing
	 */
	def boolean includeCheckpointing() {
		return !checkpointOperations.filter(CheckpointOperation).empty
	}
	
	/**
	 * Returns a list of Statements
	 */
	def ArrayList<Statement> getStatements() {
		return this.statements;
	}
	
	/**
	 * Returns a list of Function Operations
	 */
	def ArrayList<FunctionOperation> getFunctionOperations() {
		return this.functionOperations;
	}
	
	/**
	 * Returns a list of File Operations
	 */
	def ArrayList<FileOperation> getFileOperations() {
		return this.fileOperations;
	}
	
	/**
	 * Returns a list of Checkpoint Operations
	 */
	def ArrayList<CheckpointOperation> getCheckpointOperations() {
		return this.checkpointOperations;
	}

	/*
	 * END ACCESSORS
	 */

	/*
	 * START UTILS
     */
     
	/**
	 * Check if an object exists
	 */
	def private boolean exists(Object obj){
		return Util.exists(obj);
	}
	
	/**
	 * Adds a statement to the list if not already present
	 */
	def private void add(Statement statement) {
		if(!statements.contains(statement))
			statements.add(statement);
	}
	
	/*
	 * END UTILS
	 */
	 
	 /**
	  * Inner class that handles the handshake between grammar statements
	  * and generator statements. This portion is more complex than its
	  * encapsulating class because statements hold dependencies that
	  * are not explicitly stated in the grammar.
	  */
	private static class StatementRegistrar {
		private static HashMap<String, Statement> identifiers = new HashMap<String, Statement>();
		
		/**
		 * Resets the hashmap
		 */
		def static reset() {
			identifiers.clear();
		}
		
		/**
		 * Assigns a Grammar NewStatement to a Generator NewStatement
		 * Hooks the dependency that was lost in storage (see StatementRegistrar.store)
		 */
		def static NewStatement assign(gSpotVarNewStatement statement, Scope scope, ExistingStatement dependency){
			if(statement.name.exists) {
				var newStatement = identifiers.get(statement.name.name) as NewStatement;
				newStatement.setDependency(dependency);
				return newStatement;
			}
			else if(statement.statement.assignment.exists)
				return new NewAssignmentStatement(statement.statement.assignment, scope, dependency)
			else if(statement.statement.declaration.exists)
				return new NewDeclarationStatement(statement.statement.declaration, scope, dependency)
			else if(statement.statement.call.exists)
				return new NewFunctionCallStatement(statement.statement.call, scope, dependency)
			else if(statement.statement.literal.exists)
				return new NewLiteralStatement(statement.statement.literal, scope, dependency)
		}
		
		/**
		 * Assigns a Grammar ExistingStatement to a Generator ExistingStatement
		 */
		def static ExistingStatement assign(gSpotVarExistingStatement statement, Scope scope){
			if(statement.name.exists)
				return identifiers.get(statement.name.name) as ExistingStatement;
			if(statement.statement.assignment.exists)
				return new ExistingAssignmentStatement(statement.statement.assignment, scope)
			else if(statement.statement.declaration.exists)
				return new ExistingDeclarationStatement(statement.statement.declaration, scope)
			else if(statement.statement.call.exists)
				return new ExistingFunctionCallStatement(statement.statement.call, scope)
		}
		
		/**
		 * Assigns a Grammar NewStatement to a Generator NewStatement
		 * Hooks the dependency that was lost in storage (see StatementRegistrar.store)
		 */
		def static NewStatement assign(gNewStatement statement, Scope scope, ExistingStatement dependency){
			if(statement.assignment.exists)
				return new NewAssignmentStatement(statement.assignment, scope, dependency)
			else if(statement.declaration.exists)
				return new NewDeclarationStatement(statement.declaration, scope, dependency)
			else if(statement.call.exists)
				return new NewFunctionCallStatement(statement.call, scope, dependency)
			else if(statement.literal.exists)
				return new NewLiteralStatement(statement.literal, scope, dependency)
		}
		
		/**
		 * Assigns a Grammar ExistingStatement to a Generator ExistingStatement
		 */
		def static ExistingStatement assign(gExistingStatement statement, Scope scope){
			if(statement.assignment.exists)
				return new ExistingAssignmentStatement(statement.assignment, scope)
			else if(statement.declaration.exists)
				return new ExistingDeclarationStatement(statement.declaration, scope)
			else if(statement.call.exists)
				return new ExistingFunctionCallStatement(statement.call, scope)
		}
		
		/**
		 * Associates a SPOT variable with a get/new statement
		 * e.g. id = new Declaration("a") associates "id" with the declaration statement
		 * 
		 * ** WARNING **
		 * When an identifier is storing a new statement, it cannot find a dependency.
		 * This dependency should be found at operation-time and associated.
		 * This makes for the dynamic usage of new statements.
		 */
		def static void store(gSpotAssign statement, Scope scope){
			if(statement.knew.exists) 
				identifiers.put(statement.knew.name, StatementRegistrar.assign(statement.knew.statement, scope, null))
			else
				identifiers.put(statement.existing.name, StatementRegistrar.assign(statement.existing.statement, scope))
		}
		
		/**
		 * Tells whether the object exists
		 */
		def static boolean exists(Object obj) {
			return Util.exists(obj);
		}
	}
}