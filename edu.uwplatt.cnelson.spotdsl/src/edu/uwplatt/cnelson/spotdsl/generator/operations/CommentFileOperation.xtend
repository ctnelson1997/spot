/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.operations

import edu.uwplatt.cnelson.spotdsl.generator.Util
import edu.uwplatt.cnelson.spotdsl.generator.operations.types.FileOperation
import edu.uwplatt.cnelson.spotdsl.spotDsl.gSpotCommentScope

class CommentFileOperation extends FileOperation{
	
	private gSpotCommentScope data;
	
	new(gSpotCommentScope data){
		this.data = data;
	}
	
	/**
	 * Attaches a comment to a file
	 */
	override operate() '''
		�Util.mend('''attachComment(file, string("//�data.comment�"), PreprocessingInfo::CplusplusStyleComment, PreprocessingInfo::before);''')�
	'''
	
	override comment() '''
		// Attaches a comment to the file
	'''
	
}