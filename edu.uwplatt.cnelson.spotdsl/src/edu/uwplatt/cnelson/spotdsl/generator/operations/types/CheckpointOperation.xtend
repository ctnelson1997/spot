/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.operations.types

import edu.uwplatt.cnelson.spotdsl.generator.DocumentedGeneration
import edu.uwplatt.cnelson.spotdsl.generator.Scope

abstract class CheckpointOperation implements DocumentedGeneration {
	
	protected Scope scope;
	protected int frequency;
	
	new(Scope scope, int frequency){
		this.scope = scope;
		this.frequency = frequency;
	}
	
	/**
	 * Gets the scope for the checkpointing to take place
	 */
	def Scope getScope(){
		return scope;
	}
	
	/**
	 * Abstract identification function. Purpose is to identify the variables
	 * that need to be checkpointed and construct an SgStatement to checkpoint
	 * them. It then processes these statements.
	 */
	def CharSequence operate();
	
	/**
	 * Util to check if an object exists
	 */
	def protected boolean exists(Object obj){
		return (obj !== null)
	}
	
	override comment() '''
		�IF scope.FILE.wildcard && scope.FUNCTION.wildcard�
			// Checkpoints in any file and in any function
		�ELSEIF !scope.FILE.wildcard && scope.FUNCTION.wildcard�
			// Checkpoints in file "�scope.FILE.name�" and in any function			
		�ELSEIF scope.FILE.wildcard && !scope.FUNCTION.wildcard�
			// Checkpoints in any file and in function "�scope.FUNCTION.name�"
		�ELSE�
			// Checkpoints in file "�scope.FILE.name�" and in function "�scope.FUNCTION.name�"		
		�ENDIF�
	'''
} 