/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.operations;

import edu.uwplatt.cnelson.spotdsl.generator.Util
import edu.uwplatt.cnelson.spotdsl.generator.operations.types.FunctionOperation
import edu.uwplatt.cnelson.spotdsl.generator.statements.ExistingStatement
import edu.uwplatt.cnelson.spotdsl.generator.statements.NewStatement
import edu.uwplatt.cnelson.spotdsl.spotDsl.gLocation

public class PutOperation extends FunctionOperation {
	private NewStatement newStatement;
	private ExistingStatement existingStatement;
	private gLocation location;
	
	/**
	 * Constructs a put operation, requiring a new and existing statement as well as a location
	 */
	new(NewStatement newStatement, ExistingStatement existingStatement, gLocation location) {
		super(newStatement);
		this.newStatement = newStatement;
		this.existingStatement = existingStatement;
		this.location = location;
	}
	
	/**
	 * Builds a put statement by specifying whether it is before, at, or after the existing statement
	 */
	override build()'''
		�IF location.before�
			�Util.mend('''insertStatementBefore(�existingStatement.global.access�, �newStatement.global.access�);''')�
		�ELSEIF location.at�
			�Util.mend('''replaceStatement(�existingStatement.global.access�, �newStatement.global.access�);''')�
		�ELSEIF location.after�
			�Util.mend('''insertStatementAfter(�existingStatement.global.access�, �newStatement.global.access�);''')�
		�ENDIF�
	'''
	
	override comment() '''
		�IF location.before�
			// Puts the new statement before the existing one
		�ELSEIF location.at�
			// Replaces the existing statement with the new one
		�ELSEIF location.after�
			// Puts the new statement after the existing one
		�ENDIF�
	'''
	
}
