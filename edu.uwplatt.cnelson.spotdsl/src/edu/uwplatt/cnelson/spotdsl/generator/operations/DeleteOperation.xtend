/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.operations;

import edu.uwplatt.cnelson.spotdsl.generator.Util
import edu.uwplatt.cnelson.spotdsl.generator.operations.types.FunctionOperation
import edu.uwplatt.cnelson.spotdsl.generator.statements.ExistingStatement

public class DeleteOperation extends FunctionOperation{
	private ExistingStatement statement;
	
	new(ExistingStatement statement) {
		super(statement);
		this.statement = statement;
	}
	
	override build()'''
		�Util.mend('''removeStatement(�statement.global.access�);''')�
	'''
	
	override comment() '''
		// Removes the statement
	'''
	
}
