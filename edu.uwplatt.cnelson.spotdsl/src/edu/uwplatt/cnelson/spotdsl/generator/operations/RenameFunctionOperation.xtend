/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.operations

import edu.uwplatt.cnelson.spotdsl.generator.Util
import edu.uwplatt.cnelson.spotdsl.generator.operations.types.FunctionOperation
import edu.uwplatt.cnelson.spotdsl.generator.statements.existing.FunctionLocator

class RenameFunctionOperation extends FunctionOperation {
	private FunctionLocator functionLocator;
	private String oldName;
	private String newName;
	
	new(FunctionLocator functionLocator, String oldName, String newName) {
		super(functionLocator);
		this.functionLocator = functionLocator;
		this.oldName = oldName;
		this.newName = newName;
	}
	
	override build() '''
		�Util.mend('''((SgFunctionDeclaration*) �functionLocator.global.access�)->set_name(SgName("�newName�"));''')�
		
		// Quick hotfix for ROSE not updating names in references
		Rose_STL_Container<SgNode*> nodeList = NodeQuery::querySubTree (project, V_SgFunctionRefExp);
		Rose_STL_Container<SgNode*>::iterator j = nodeList.begin();
		while(j != nodeList.end()) {
			SgFunctionRefExp* ref = isSgFunctionRefExp(*j);
			if(ref != NULL)
				if(ref->get_symbol()->get_declaration()->get_name().getString() == "�oldName�")
					ref->get_symbol()->get_declaration()->set_name(SgName("�newName�"));
			j++;
		}
	'''
	
	override comment() '''
		�Util.mend('''// Renames the function to �newName�''')�
	'''
}