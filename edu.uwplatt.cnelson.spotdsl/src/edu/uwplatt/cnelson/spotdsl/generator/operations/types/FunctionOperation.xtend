/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.operations.types

import edu.uwplatt.cnelson.spotdsl.generator.DocumentedGeneration
import edu.uwplatt.cnelson.spotdsl.generator.Util
import edu.uwplatt.cnelson.spotdsl.generator.statements.Statement

abstract class FunctionOperation implements DocumentedGeneration {
	
	private Statement vectorRef;
	
	/**
	 * Constructs an operation with the required reference statement
	 */
	new(Statement vectorRef){
		this.vectorRef = vectorRef;
	}
	
	/**
	 * Performs the operation
	 */
	def operate()'''
		�Util.buildForLoop(vectorRef, build)�
	'''
	
	/**
	 * Provides the accessor for the vector
	 */
	def protected access(CharSequence str)'''
		�str�[i]
	'''
	
	/**
	 * Required build statement for the operate function
	 */
	def CharSequence build();
}