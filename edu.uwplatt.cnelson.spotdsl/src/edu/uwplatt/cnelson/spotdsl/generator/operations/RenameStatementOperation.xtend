/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.operations

import edu.uwplatt.cnelson.spotdsl.generator.Util
import edu.uwplatt.cnelson.spotdsl.generator.operations.types.FunctionOperation
import edu.uwplatt.cnelson.spotdsl.generator.statements.existing.FunctionLocator

class RenameStatementOperation extends FunctionOperation {
	
	private FunctionLocator functionLocator;
	private String oldName;
	private String newName;
	
	new(FunctionLocator functionLocator, String oldName, String newName) {
		super(functionLocator);
		this.functionLocator = functionLocator;
		this.oldName = oldName;
		this.newName = newName;
	}
	
	override build() '''
		�Util.mend('''SgVariableSymbol* varSymb = lookupVariableSymbolInParentScopes(*new SgName("�oldName�"), �functionLocator.global.access�->get_scope());''')�
		
		if(varSymb == NULL)
			continue;
		
		�Util.mend('''varSymb->get_declaration()->set_name(SgName("�newName�"));''')�
	'''
	
	override comment() '''
		�Util.mend('''// Renames the variable from �oldName� to �newName�''')�
	'''
	
}