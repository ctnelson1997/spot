/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotdsl.generator.operations.checkpoint

import edu.uwplatt.cnelson.spotdsl.generator.Scope
import edu.uwplatt.cnelson.spotdsl.generator.Util
import edu.uwplatt.cnelson.spotdsl.generator.operations.types.CheckpointOperation

class ManualCheckpointOperation extends CheckpointOperation {
	
	protected String[] names;
	
	/**
	 * Stores the variable names specified for checkpointing.
	 * Sends construction information upwards.
	 */
	new(Scope scope, String[] names, int frequency) {
		super(scope, frequency);
		this.names = names;
	}
	
	override operate()'''
		�FOR name : names�
			�Util.mend('''checkpoint(blocks, "�name�", �frequency�);''')�
		�ENDFOR�
	'''
	
}