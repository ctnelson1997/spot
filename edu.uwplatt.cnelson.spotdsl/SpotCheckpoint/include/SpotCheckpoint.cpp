/*
*  SPOT: Abstraction in Metaprogramming
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*/

#include <string>
#include <fstream>
#include <ctime>

using namespace std;

class SpotCheckpoint

{
public:
   template<class T>
   static void save(T var, string name) {
      // Credit @ https://stackoverflow.com/questions/16357999/current-date-and-time-as-string
      time_t rawtime;
      struct tm * timeinfo;
      char buffer[80];

      time(&rawtime);
      timeinfo = localtime(&rawtime);

      strftime(buffer, sizeof(buffer), "%d-%m-%Y %I:%M:%S", timeinfo);
      string strTime(buffer);
      // End Credit

      ofstream file;

      file.open((path + "SPOTCheckpoint.txt").c_str(), ios_base::app);
      file << strTime << " " << name << " = " << var << endl;
      file.close();
   }

   void setPath(string p) {
     path = p;
   }

private:
   static string path;
};

string SpotCheckpoint::path = "/home/spot/Desktop/";

