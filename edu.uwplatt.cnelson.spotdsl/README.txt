--------------------------------
   _____ _____   ____ _______ 
  / ____|  __ \ / __ \__   __|
 | (___ | |__) | |  | | | |   
  \___ \|  ___/| |  | | | |   
  ____) | |    | |__| | | |   
 |_____/|_|     \____/  |_|   
                              
--------------------------------


-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-

  University of Wisconsin-Platteville
    Department of Computer Science & Software Engineering
      Dr. Songqing (Joshua) Yue                              (yues@uwplatt.edu)
      Cole Nelson                                            (nelsoncole@uwplatt.edu)

  Provided underneath a grant funded by the Office of Research and Sponsored Programs

-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-

1. License
--------------------------------
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See "LICENSE.txt" for further info.

2. About
--------------------------------
SPOT is a domain-specific language that utilizes a high level of abstraction to perform source-to-source transformations on C/C++ source code. SPOT seeks to simplify concepts of metaprogramming and compiler theory by providing functionality to manipulate statements, functions, and files. Such manipulations are done through the generation and execution of code using the ROSE Compiler, which constructs, modifies, and deconstructs an abstract syntax tree.

3. Installation
--------------------------------
SPOT is available as an Ubuntu 16.04 virtual machine image. The installation process can be found in the User Manual located at \Documentation\Final\PDF\SPOT User Manual.pdf

4. Examples
--------------------------------
Example transformations can be seen within the User Manual listed above, or within the VM in the Eclipse workspace. Sample C++ files are also provided on the desktop under "example input files".

5. ROSE Compiler
--------------------------------
SPOT uses the ROSE Compiler to perform its source-to-source transformations. More information about the ROSE Compiler can be found at http://rosecompiler.org.