/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotcompiler;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

public class Compiler {

	// Translator Name
	private static final String TRANSLATOR_NAME       = "translator";

	// Include Directories
	private static final String ROSE_INCLUDE_DIR      = "/home/spot/opt/rose_inst/include/rose";

	// Library Directories
	private static final String ROSE_LIB_DIR          = "/home/spot/opt/rose_inst/lib";
	private static final String BOOST_LIB_DIR         = "/home/spot/opt/boost/1.61.0/gcc-4.9.3-default/lib";
	private static final String YAML_LIB_DIR          = "/home/spot/opt/yaml/lib";
	private static final String SPOT_LIB_DIR          = "/home/spot/opt/spot/lib";

	// Library Names
	private static final String ROSE_LIB_NAME         = "rose";
	private static final String BOOST_LIB_NAME        = "boost_system";
	private static final String YAML_LIB_NAME         = "yaml-cpp";
	private static final String SPOT_LIB_NAME         = "SpotCheckpoint";
	
	// Shell Parameters
	private static final String SHELL                 = "/bin/sh";
	private static final String LIBTOOL               = "/home/spot/build-rose/libtool";
	private static final String CXX                   = "g++-4.9";
	private static final String CPPFLAGS              = "";
	private static final String CXXFLAGS              = "-g -rdynamic -Wall -Wno-unused-local-typedefs -Wno-attributes -g -rdynamic -O0 -Wall -Wno-unused-local-typedefs -Wno-attributes -Wall -Wno-unused-local-typedefs -Wno-attributes";
	private static final String LDFLAGS               = "";
	private static final String REMOVE                = "rm";
	
	private static final String MOVED_SOURCE          = "/home/spot/compiler/currentTransformations.c";
	
	private String source;
	private String outputLocation;
	private File[] files;

	/**
	 * Creates a compiler to make a SPOT transformation
	 * @param source file for Transformations.c. Found in Eclipse workspace
	 * @param outputLocation directory to ouput transformed files
	 * @param files source files to transform
	 * @throws IOException 
	 */
	public Compiler(String source, String outputLocation, File[] files) {
		this.source = source;
		this.outputLocation = outputLocation;
		this.files = files;
	}
	
	/**
	 * Compiles and executes a SPOT transformation
	 * @throws IOException thrown from dispatching a command to the shell
	 * @throws InterruptedException thrown from dispatching a command to the shell
	 */
	public void execute() throws IOException, InterruptedException {
		this.moveSource(source);
		this.printHeader();
		this.compile();
		this.link();
		this.translate();
		this.finish();
	}
	
	/**
	 * Moves the source to a non-whitespace path
	 * @param source originating source
	 * @throws IOException thrown from moving files
	 */
	private void moveSource(String source) throws IOException {
		Path sourcePath = Paths.get(new File(source).getAbsolutePath());
		Files.copy(sourcePath, Paths.get(new File(MOVED_SOURCE).getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
	}
	
	/**
	 * Prints a header with its internals to console
	 */
	private void printHeader() {
		Console.print();
		Console.print("Beginning transformation...");
		Console.print("Transformer File: " + source);
		Console.print("Output Location: " + outputLocation);
		Console.print("Source Files:");
		for(File file : files)
			Console.print(" + " + file.getPath());
		Console.print();
	}
	
	/**
	 * Compiles the Transformations.c (source) file with ROSE
	 * @throws IOException thrown from dispatching a command to the shell
	 * @throws InterruptedException thrown from dispatching a command to the shell
	 */
	private void compile() throws IOException, InterruptedException {
		Console.print("Compiling transformer...");
		this.dispatch(SHELL + " " + LIBTOOL + " --mode=compile " + CXX +  " " + CXXFLAGS + " " +  CPPFLAGS + " -I"
				+ ROSE_INCLUDE_DIR + " -c -o " + TRANSLATOR_NAME + ".lo " + MOVED_SOURCE + " -ggdb");
		Console.print();
	}
	
	/**
	 * Links objects from the Transformations.c to create the translator
	 * @throws IOException thrown from dispatching a command to the shell
	 * @throws InterruptedException thrown from dispatching a command to the shell
	 */
	private void link() throws IOException, InterruptedException {
		Console.print("Linking compiled transformer objects...");
		this.dispatch(SHELL + " " + LIBTOOL + " --mode=link " + CXX + " " + CXXFLAGS + " " + LDFLAGS
				+ " -DBOOST_SYSTEM_NO_DEPRECATED -L" + ROSE_LIB_DIR + " -L" + BOOST_LIB_DIR + " -L" + YAML_LIB_DIR
				+ " -L" + SPOT_LIB_DIR + " -o " + TRANSLATOR_NAME + " " + TRANSLATOR_NAME + ".lo -l" + ROSE_LIB_NAME
				+ " -l" + BOOST_LIB_NAME + " -l" + YAML_LIB_NAME + " -l" + SPOT_LIB_NAME);
		Console.print();
	}
	
	/**
	 * Executes the translator on the source files to perform the transformation
	 * This step takes the longest time -- ROSE must construct, modify, and deconstruct the AST
	 * @throws IOException thrown from dispatching a command to the shell
	 * @throws InterruptedException thrown from dispatching a command to the shell
	 */
	private void translate() throws IOException, InterruptedException {
		Console.print("Performing translation of source files...");
		String[] command = new String[files.length + 1];
		command[0] = "./" + TRANSLATOR_NAME;
		for(int i = 0; i < files.length; i++)
			 command[i+1] = files[i].getAbsolutePath();
		this.dispatch(command);
		Console.print();
	}
	
	/**
	 * Renames the files and moves them to the output location. Then cleans its working directory.
	 * @throws IOException thrown from dispatching a command to the shell
	 * @throws InterruptedException thrown from dispatching a command to the shell
	 */
	private void finish() throws IOException, InterruptedException {
		Console.print("Moving transformed files to output location: " + outputLocation);
		
		ArrayList<String> names = new ArrayList<>();
		for(File file : files)
			names.add(file.getName());
		
		Path newDir = Paths.get(outputLocation);
		for(String name : names) { // renames and moves transformed files
			Path source = Paths.get(new File("rose_" + name).getAbsolutePath());
			Files.move(source, source.resolveSibling("spot_" + name), StandardCopyOption.REPLACE_EXISTING);
			Path renamedSource = Paths.get(new File("spot_" + name).getAbsolutePath());
			Files.move(renamedSource, newDir.resolve(renamedSource.getFileName()), StandardCopyOption.REPLACE_EXISTING);
		}
		Console.print();
		
		Console.print("Cleaning up files..."); // cleans working directory
		this.dispatch(REMOVE + " -f " + TRANSLATOR_NAME);
		this.dispatch(REMOVE + " -f " + TRANSLATOR_NAME + ".o");
		this.dispatch(REMOVE + " -f " + TRANSLATOR_NAME + ".lo");
		this.dispatch(REMOVE + " -f " + TRANSLATOR_NAME + ".o");
		for(String name : names) {
			this.dispatch(new String[] {REMOVE, "-f", name.split("\\.")[0] + ".o"});
			this.dispatch(new String[] {REMOVE, "-f", name.split("\\.")[0] + ".ti"});
		}
		this.dispatch(REMOVE + " -f a.out");
		Console.print();
	}
	
	/**
	 * Passes a command into the shell for process to handle
	 * @param command command to pass into the shell
	 * @throws IOException thrown from dispatching a command to the shell
	 * @throws InterruptedException thrown from dispatching a command to the shell
	 */
	private void dispatch(String command) throws IOException, InterruptedException {
		Console.print("Dispatching command into shell: " + command);
		this.process(Runtime.getRuntime().exec(command));
	}
	
	/**
	 * Passes a command into the shell for process to handle
	 * @param command command to pass into the shell
	 * @throws IOException thrown from dispatching a command to the shell
	 * @throws InterruptedException thrown from dispatching a command to the shell
	 */
	private void dispatch(String[] command) throws IOException, InterruptedException {
		Console.print("Dispatching command into shell: " + this.concat(command));
		this.process(Runtime.getRuntime().exec(command));
	}
	
	/**
	 * Handles errors from the process through completion
	 * @param proc process to monitor
	 * @throws InterruptedException thrown from dispatching a command to the shell
	 */
	private void process(Process proc) throws InterruptedException {
		new Thread(new SpotStream(proc.getInputStream())).start();
		new Thread(new SpotStream(proc.getErrorStream())).start();
		proc.waitFor();
	}
	
	/**
	 * Concats an array of string's since we can't use Java 8's library at runtime (runtime is SE7)
	 * @param args array of strings to concat
	 * @return command as a single string
	 */
	private String concat(String[] args) {
		String command = "";
		for(String arg : args)
			command += " " + arg;
		return command.replaceFirst(" ", "");
	}
}
