/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotcompiler;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

public class CompilerMenu {

	private JFrame frmSpotCompiler;
	private JTextField txt_Output;
	private JList<File> list_Files;
	private DefaultListModel<File> model_Files;
	private DefaultComboBoxModel<String> combo_model = new DefaultComboBoxModel<>();
	private JComboBox<String> combo_ProjectName = new JComboBox<>(combo_model);
	
	private File outputLocation = null;

	/**
	 * Initialize console and launch the application.
	 */
	public static void main(String[] args) {
		Console.init();
		new CompilerMenu();
	}

	/**
	 * Start the Eclipse Project Name getter and boot the GUI
	 */
	public CompilerMenu() {
		new Thread(new EclipseProjectUpdater(combo_model)).start();
		setupTheme();
		initialize();
		frmSpotCompiler.setVisible(true);
		Console.print("SPOT Compiler started!");
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSpotCompiler = new JFrame();
		frmSpotCompiler.setTitle("SPOT Compiler");
		frmSpotCompiler.setBounds(100, 100, 481, 330);
		frmSpotCompiler.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSpotCompiler.getContentPane().setLayout(null);
		
		JLabel lbl_Header = new JLabel("SPOT Compiler");
		lbl_Header.setFont(new Font("Tahoma", Font.BOLD, 16));
		lbl_Header.setBounds(12, 13, 154, 20);
		frmSpotCompiler.getContentPane().add(lbl_Header);
		
		JLabel lbl_EclipseProjectName = new JLabel("Eclipse Project Name");
		lbl_EclipseProjectName.setBounds(22, 39, 157, 16);
		frmSpotCompiler.getContentPane().add(lbl_EclipseProjectName);
		
		list_Files = new JList<>();
		model_Files = new DefaultListModel<>();
		list_Files.setModel(model_Files);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(list_Files);
		scrollPane.setBounds(12, 103, 312, 93);
		frmSpotCompiler.getContentPane().add(scrollPane);
		
		JLabel lbl_Reminder = new JLabel("Make sure to save your Eclipse project before transforming!");
		lbl_Reminder.setBounds(12, 237, 450, 16);
		frmSpotCompiler.getContentPane().add(lbl_Reminder);
		
		JButton btn_AddList = new JButton("Add");
		btn_AddList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				doAdd();
			}
		});
		btn_AddList.setBounds(337, 114, 97, 25);
		frmSpotCompiler.getContentPane().add(btn_AddList);
		
		JButton btn_RemoveList = new JButton("Remove");
		btn_RemoveList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				doRemove();
			}
		});
		btn_RemoveList.setBounds(337, 152, 97, 25);
		frmSpotCompiler.getContentPane().add(btn_RemoveList);
		
		JLabel lbl_Specify = new JLabel("Specify the file(s) to transform below");
		lbl_Specify.setBounds(12, 74, 312, 16);
		frmSpotCompiler.getContentPane().add(lbl_Specify);
		
		JButton btn_Transform = new JButton("Transform");
		btn_Transform.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doTransform();
			}
		});
		btn_Transform.setBounds(22, 266, 302, 25);
		frmSpotCompiler.getContentPane().add(btn_Transform);
		
		JLabel lbl_Output = new JLabel("Output Location");
		lbl_Output.setBounds(22, 209, 120, 16);
		frmSpotCompiler.getContentPane().add(lbl_Output);
		
		txt_Output = new JTextField();
		txt_Output.setEditable(false);
		txt_Output.setBounds(140, 206, 179, 22);
		frmSpotCompiler.getContentPane().add(txt_Output);
		txt_Output.setColumns(10);
		
		JButton btn_Browse = new JButton("Browse");
		btn_Browse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doBrowse();
			}
		});
		btn_Browse.setBounds(331, 205, 97, 25);
		frmSpotCompiler.getContentPane().add(btn_Browse);
		
		combo_ProjectName.setBounds(187, 35, 212, 24);
		frmSpotCompiler.getContentPane().add(combo_ProjectName);
	}
	
	/**
	 * Sets the Ubuntu look and feel
	 */
	private void setupTheme() {
		try {
	    	UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    } catch (Throwable ex) {
	    	Console.print("Could not set Ubuntu Look and Feel :(");
	    }
	}
	
	/**
	 * Browser for choosing the output directory
	 */
	private void doBrowse() {
		JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if(chooser.showOpenDialog(null) != JFileChooser.APPROVE_OPTION)
            return;
        File file = chooser.getSelectedFile();
        outputLocation = file;
        txt_Output.setText(file.getPath());
	}
	
	/**
	 * Browser for choosing the source files
	 */
	private void doAdd() {
		JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("C/C++ Source Files", "c", "cpp", "h", "hpp");
        chooser.setFileFilter(filter);
        chooser.setMultiSelectionEnabled(true);
        if(chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
        	for(File file : chooser.getSelectedFiles())
        		if(!model_Files.contains(file))
        				model_Files.addElement(file);
	}
	
	/**
	 * Removes the selected files from the transformation list
	 */
	private void doRemove() {
		List<File> files = list_Files.getSelectedValuesList();
		for(File file : files)
			model_Files.removeElement(file);
	}
	
	/**
	 * Checks for any obvious user errors and sends information to the compiler
	 */
	private void doTransform() {
		
		/*
		 * Start obvious error checking
		 */
		
		String projName = (String) combo_model.getSelectedItem();
		if(projName == null) {
			Console.print("Require a SPOT Eclipse Project");
			JOptionPane.showMessageDialog(null, "Please specify a SPOT Eclipse Project.");
			return;
		}
		
		List<File> inputFiles = Collections.list(model_Files.elements());
		if(inputFiles.isEmpty()) {
			Console.print("Require Input Files");
			JOptionPane.showMessageDialog(null, "Please specify input files to transform.");
			return;
		}
		
		if(outputLocation == null) {
			Console.print("Require Output Location");
			JOptionPane.showMessageDialog(null, "Please specify an output directory.");
			return;
		}
		
		/*
		 * End obvious error checking
		 */
		
		try { // attempts a compilation
			new Compiler("/home/spot/eclipse-workspace/" + projName + "/src-gen/Transformations.c",
					txt_Output.getText() + "/",
					inputFiles.toArray(new File[0]))
			.execute();
		} catch(Exception e) {
			Console.print("Warning: Errors occured during compilation. See log for details.");
			Console.printLogAccessInstructions();
			JOptionPane.showMessageDialog(null, "Warning: Errors occured during compilation. See log for details.");
			return;
		}
		
		Console.print("Transformation completed successfully!");
		Console.printLogAccessInstructions();
		JOptionPane.showMessageDialog(null, "Transformation completed successfully!");
	}
}
