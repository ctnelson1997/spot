/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotcompiler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Console {
	
	private static final SimpleDateFormat FORMAT      = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final String PREFIX                = " [SpotCompiler] ";
	private static final String LOG_FILE              = "/home/spot/compiler/logs/" + FORMAT.format(new Date()).replaceAll(" ", "_") + ".log";
	private static PrintWriter logWriter;
	
	/**
	 * Initializes the console by creating the log file and initializing the writer
	 */
	public static void init() {
		try {
			File dir = new File("/home/spot/compiler/logs");
			if(!dir.exists())
				dir.mkdir();
			logWriter = new PrintWriter(LOG_FILE, "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			Console.print("Could not create a log!");
		}
		
		Runtime.getRuntime().addShutdownHook(new Thread() { // saves file on close
			@Override
			public void run() {
				logWriter.close();
			}
		});
	}
	
	/**
	 * Returns the path of the log file
	 * @return path of the log file
	 */
	public static String getLog() {
		return Console.LOG_FILE;
	}
	
	/**
	 * Prints a string to the terminal and log file with formatting
	 * @param str the string to print and log
	 */
	public static void print(String str) {
		String print = FORMAT.format(new Date()) + PREFIX + str;
		System.out.println(print);
		logWriter.println(print);
	}
	
	/**
	 * Prints a blank line to terminal and log file
	 */
	public static void print() {
		System.out.println();
		logWriter.println();
	}
	
	/**
	 * End of compilation message
	 */
	public static void printLogAccessInstructions() {
		System.out.println("Log file can be found at " + Console.getLog());
		System.out.println("Note that you must close the compiler before accessing the log file.");
	}
}
