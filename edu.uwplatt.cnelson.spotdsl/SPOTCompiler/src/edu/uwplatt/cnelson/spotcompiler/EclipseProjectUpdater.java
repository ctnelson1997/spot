/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotcompiler;

import java.io.File;

import javax.swing.DefaultComboBoxModel;

public class EclipseProjectUpdater implements Runnable {

	private static final File WORKSPACE = new File("/home/spot/eclipse-workspace/");
	
	private DefaultComboBoxModel<String> combo;
	
	/**
	 * Links the updater to the GUI selector
	 * @param combo combo to sync with
	 */
	public EclipseProjectUpdater(DefaultComboBoxModel<String> combo) {
		this.combo = combo;
	}
	
	/**
	 * Checks the filesystem for valid SPOT projects for the GUI selector
	 */
	@Override
	public void run() {
		while(true) {
			try {				
				for(File project : WORKSPACE.listFiles())
					if(EclipseProjectUpdater.isValid(project) && combo.getIndexOf(project.getName()) == -1)
						combo.addElement(project.getName());
				Thread.sleep(1000);
			} catch (InterruptedException e) { /* Suppressed Output */ }
		}
	}
	
	/**
	 * Validates that an Eclipse Project is a SPOT Project
	 * @param project Eclipse project to validate
	 * @return true if SPOT, false if not
	 */
	private static boolean isValid(File project) {
		if(new File(project.getAbsolutePath() + "/src-gen/Transformations.c").exists())
			return true;
		return false;
	}

}
