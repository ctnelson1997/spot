/*
 *  SPOT: Abstraction in Metaprogramming
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 */

package edu.uwplatt.cnelson.spotcompiler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class SpotStream implements Runnable {

	private InputStream input;
	
	/**
	 * Creates a new stream that can be run concurrently
	 * @param input InputStream to read from
	 */
	public SpotStream(InputStream input) {
		this.input = input;
	}
	
	/**
	 * Writes the output from the stream to the console
	 */
	@Override
	public void run() {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));
			String line;
			while((line = reader.readLine()) != null)
				Console.print(line);
			Thread.currentThread().join();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) { /* Suppressed Output */ }
	}

}
